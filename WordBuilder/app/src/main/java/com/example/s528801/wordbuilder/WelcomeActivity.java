package com.example.s528801.wordbuilder;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class WelcomeActivity extends AppCompatActivity {

    public static int Lcourse = -1;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
    }
    public void startlevel(View v)
    {
        Intent startlevel = new Intent(this,LevelLearning.class);

        switch (v.getId()){
            case R.id.begi1:
                startlevel.putExtra("Lcourse",1);
                Lcourse = 1;
                break;
            case R.id.begi2:
                startlevel.putExtra("Lcourse",2);
                Lcourse = 2;
                break;
            case R.id.begi3:
                startlevel.putExtra("Lcourse",3);
                Lcourse = 3;
                break;
        }
        startActivity(startlevel);
    }
}
