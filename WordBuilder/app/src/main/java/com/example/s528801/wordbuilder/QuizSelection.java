package com.example.s528801.wordbuilder;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class QuizSelection extends AppCompatActivity {
    public static int course = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz_selection);
    }
    public void startlevel(View v)
    {
        Intent startlevel = new Intent(this,QuizLevel.class);
        switch (v.getId()){
            case R.id.begi1:
                startlevel.putExtra("course",1);
                course = 1;
                break;
            case R.id.begi2:
                startlevel.putExtra("course",2);
                course = 2;
                break;
            case R.id.begi3:
                startlevel.putExtra("course",3);
                course = 3;
                break;
        }

        startActivity(startlevel);
    }




}
