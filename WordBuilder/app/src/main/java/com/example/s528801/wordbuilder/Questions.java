package com.example.s528801.wordbuilder;

/**
 * Created by S528767 on 10/26/2017.
 */

public class Questions {

    private String mQuestions [] = {
            "aboriginal", "abject", "cerebral","douse","chalice","Morose","Vacillate","Impromptu","Attenuate","Halting",
            "Convoke","Nonchalant","Reprove","Repartee","Deride","Shun","Jovial","Limber","Chic","Throng","Sartorial",
            "outstrip","Engross","outwit","Hasten","Paragon","Stodgy","Guffaw","Babble","Effervesce","Salient","Grating",
            "Solicitous","travail","Belabor","Puerile","abyss","Accessible","merit","Biting","Eschew","Verbatim","Crass",
            "Primordial","Rebus","apogee","aplomb","Poise","apiarist","hive","Finicky","Curtail","Masticate","Arraign",
            "Arid","Bigotry","Blanch","Dazzle","Screech","Bolt","Drizzle","Parsimony","Incessant","Delve","Rein","Lofty",
            "Obsequious","Torpid","Anterior","Heirloom","Wily","Pliant","Hilarity","Commodious","Pithy","Warp","consecrate",
            "crystallize","debonair","curmudgeon","Dearth","dummy"};

    private String mChoices [][] = {
            {"being the first of its kind in a region", "unsuccessful", "cancel"},
            {"lacking pride","put an end to","tending to grind down"},
            {"intellectual","working of the brain","stoppage"},
            {"group of young birds","plunge into water","ugly woman"},
            {"consecrated cup","joking","competition"},
            {"cheerful","gloomy","hurry"},
            {"to oscillate","to avoid","A witty reply"},
            {"crowd","mode of excellence","with no preparation"},
            {"to reduce","lacking pride","assist usually"},
            {"flexible","pause","to call together"},
            {"scold","gloomy","to call together"},
            {"unconcerned","swing","hurry"},
            {"occupy","scold","split"},
            {"a witty reply","tailoring","flexible"},
            {"redicule","cheerful","Stop"},
            {"cancel","take oath","first of its kind in a region"},
            {"Cheerful","put an end","trending"},
            {"flexible","stoppage","joking"},
            {"stylish","hurry","swing"},
            {"crowd","joking","to call together"},
            {"cheerful","tailoring","hurry"},
            {"avoid","outrun","witty reply"},
            {"absorb","crowd","a mode of excellence"},
            {"outsmart","pride","put an end"},
            {"hurry","flexible","call to together"},
            {"absorb","crowd","model of excellence"},
            {"stuffy","labor","laughter"},
            {"organic","puzzle","laughter"},
            {"meaningless talk","prominent","bubble"},
            {"bubble","anxious","cheerful"},
            {"prominent","careful","anxious"},
            {"irritate","stop","cheerful"},
            {"careful","laughter","meaningless talk"},
            {"labor","work for a aburb length","hiss"},
            {"labor","work for a aburb length","hiss"},
            {"enlarge","childish","bad quality"},
            {"agree","enormous chasm","bottomless"},
            {"easy to approach","vast bottomless pit","bottomless"},
            {"bad quality","juvenile","good quality deserving praise"},
            {"vinegary","bitter","sharply painful to the body or mind"},
            {"to enlarge","to avoid","meaningless talk"},
            {"a burst of deep laughter","cheerful","in exactly the same words"},
            {"a type of puzzle","bee-keeping","extremely unrefined"},
            {"widely believed but untrue","Original","hiss"},
            {"a type of puzzle","difficult to please","to entrust"},
            {"stroke","dismay","highest point"},
            {"prophetic","composure in difficult situations","pithy"},
            {"difficult to please","to entrust","good judgment with composure"},
            {"to bubble","person who keeps bees","flexible"},
            {"summit","extremely unrefined","box for bees"},
            {"bee-keeping","extremely unrefined","difficult to please"},
            {"to work at absurd length","flexible","cut short"},
            {"basic","to chew","to bubble"},
            {"to stop","cheerful","charge in court"},
            {"operatic solo","unproductive","fleet of warships"},
            {"stubborn intolerance","fragrant","barren"},
            {"loud or harsh","soothing or mild","make white or pale"},
            {"flattery","amaze","loud or harsh"},
            {"a burst of deep laughter","unpleasant high sharp sound","to stop"},
            {"fastening pin or screw","fleet of warships","fragrant"},
            {"guidance ","a fine, misty rain","inactive"},
            {"uninterrupted","adaptable","extreme economy or stinginess"},
            {"multitude","uninterrupted","a means of restraint"},
            {"multitude","do research","inactive"},{"fragrant","operatic solo","guidance"},
            {"roomy","elevated in style","means of restraint"},
            {"trying to wim favor from people by flattery","great merriment","inactive"},
            {"multitude","inactive","guidance"},{"placed in front","a pause","enthusiasm"},
            {"roomy","article passed down through generations","elevated in style"},{"operatic solo","fragrant","cunning"},
            {"support","flexible","violent"},{"great merriment","barren","operatic solo"},{"roomy","multitude","inactive"},
            {"precisely","misty rain","cold"},{"twist of shape","existing within","inactive"},
            {"to intimidate","to confirm","set apart for a high purpose"},
            {"clear shape","to intimidate","lend support to"},
            {"sophisticated charm","pathetically cowardly","mysterious"},{"lose dignity","insult","shortage"},
            {"abundant supply","usually intentionally","surly person"},
            {"a","b","c"}

    };

    private String mCorrectAnswers[] =
            {"being the first of its kind in a region","lacking pride","intellectual","plunge into water","consecrated cup","gloomy","to oscillate","with no preparation",
                    "to reduce","pause","to call together","unconcerned","scold","a witty reply","redicule","first of its kind in a region",
                    "Cheerful","flexible","stylish","crowd","tailoring","outrun","absorb","outsmart","hurry","model of excellence","stuffy",
                    "laughter","meaningless talk","bubble","prominent","irritate","careful","labor","work for a aburb length","childish",
                    "vast bottomless pit","easy to approach","good quality deserving praise","sharply painful to the body or mind",
                    "to avoid","in exactly the same words","extremely unrefined","Original","a type of puzzle","highest point","composure in difficult situations",
                    "good judgment with composure","person who keeps bees","box for bees","difficult to please","cut short","to chew","charge in court","unproductive",
                    "stubborn intolerance","make white or pale","amaze","unpleasant high sharp sound","fastening pin or screw","a fine, misty rain",
                    "extreme economy or stinginess","uninterrupted","do research","guidance","elevated in style","trying to wim favor from people by flattery",
                    "inactive","placed in front","article passed down through generations","cunning","flexible","great merriment","roomy",
                    "precisely","twist of shape","set apart for a high purpose","clear shape","sophisticated charm","shortage","surly person","b"};

    public String getQuestion(int a) {
        String question = mQuestions[a];
        return question;
    }


    public String getChoice1(int a) {
        String choice0 = mChoices[a][0];
        return choice0;
    }


    public String getChoice2(int a) {
        String choice1 = mChoices[a][1];
        return choice1;
    }

    public String getChoice3(int a) {
        String choice2 = mChoices[a][2];
        return choice2;
    }

    public String getCorrectAnswer(int a) {
        String answer = mCorrectAnswers[a];
        return answer;
    }

}
