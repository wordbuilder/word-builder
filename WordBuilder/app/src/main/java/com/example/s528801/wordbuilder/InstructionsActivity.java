package com.example.s528801.wordbuilder;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class InstructionsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_instructions);
    }
    public void goBack(View v)
    {
        Intent goback = new Intent(this,StartActivity.class);
        goback.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        goback.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(goback);

    }
}
