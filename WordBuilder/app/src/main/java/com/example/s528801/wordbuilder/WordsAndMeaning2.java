package com.example.s528801.wordbuilder;

/**
 * Created by S528809 on 11/17/2017.
 */

public class WordsAndMeaning2 {
    public String word;
    public String meaning;

    @Override
    public String toString() {
        return "WordsAndMeanings{" +
                "word='" + word + '\'' +
                ", meaning='" + meaning + '\'' +
                '}';
    }
}
