package com.example.s528801.wordbuilder;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class QuizLevel extends AppCompatActivity {


    public static int level = -1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz_level);
    }
    public void quizlevel(View v)
    {

        Intent startquiz = new Intent(this,QuizActivity.class);
        int course = QuizSelection.course;
        int levelqq= v.getId();


     switch (course) {
      case 1:
         if (levelqq == R.id.level1)
         {
             startquiz.putExtra("level", 1);
             level = 1;
         }
         else  if (levelqq == R.id.level2)
         {
             startquiz.putExtra("level", 2);
             level = 2;
         }
         else  if (levelqq == R.id.level3)
         {
             startquiz.putExtra("level", 3);
             level = 3;
         }

         else  if (levelqq == R.id.level4)
         {
             startquiz.putExtra("level", 4);
             level = 4;
         }


         else  if (levelqq == R.id.level5)
         {
             startquiz.putExtra("level", 5);
             level = 5;
         }

         else  if (levelqq == R.id.level6)
         {
             startquiz.putExtra("level", 6);
             level = 6;
         }

         else  if (levelqq == R.id.level7)
         {
             startquiz.putExtra("level", 7);
             level = 7;
         }
         else  if (levelqq == R.id.level8)
         {
             startquiz.putExtra("level", 8);
             level = 8;
         }
         else  if (levelqq == R.id.level9)
         {
             startquiz.putExtra("level", 9);
             level = 9;
         }

         break;

         case 2:

             if (levelqq == R.id.level1)
             {
                 startquiz.putExtra("level", 10);
                 level = 10;
             }

             else if (levelqq == R.id.level2)
             {
                 startquiz.putExtra("level", 11);
                 level = 11;
             }
             else  if (levelqq == R.id.level3)
             {
                 startquiz.putExtra("level", 12);
                 level = 12;
             }

             else  if (levelqq == R.id.level4)
             {
                 startquiz.putExtra("level", 13);
                 level = 13;
             }


             else  if (levelqq == R.id.level5)
             {
                 startquiz.putExtra("level", 14);
                 level = 14;
             }

             else  if (levelqq == R.id.level6)
             {
                 startquiz.putExtra("level", 15);
                 level = 15;
             }

             else  if (levelqq == R.id.level7)
             {
                 startquiz.putExtra("level", 16);
                 level = 16;
             }
             else  if (levelqq == R.id.level8)
             {
                 startquiz.putExtra("level", 17);
                 level = 17;
             }
             else  if (levelqq == R.id.level9)
             {
                 startquiz.putExtra("level", 18);
                 level = 18;
             }

             break;

         case 3:
             if (levelqq == R.id.level1)
             {
                 startquiz.putExtra("level", 19);
                 level = 19;
             }
             else  if (levelqq == R.id.level2)
             {
                 startquiz.putExtra("level", 20);
                 level = 20;
             }
             else  if (levelqq == R.id.level3)
             {
                 startquiz.putExtra("level", 21);
                 level = 21;
             }

             else  if (levelqq == R.id.level4)
             {
                 startquiz.putExtra("level", 22);
                 level = 22;
             }


             else  if (levelqq == R.id.level5)
             {
                 startquiz.putExtra("level", 23);
                 level = 23;
             }

             else  if (levelqq == R.id.level6)
             {
                 startquiz.putExtra("level", 24);
                 level = 24;
             }

             else  if (levelqq == R.id.level7)
             {
                 startquiz.putExtra("level", 25);
                 level = 25;
             }
             else  if (levelqq == R.id.level8)
             {
                 startquiz.putExtra("level", 26);
                 level = 26;
             }
             else  if (levelqq == R.id.level9)
             {
                 startquiz.putExtra("level", 27);
                 level = 27;
             }

             break;

     }

        startActivity(startquiz);
    }
}


