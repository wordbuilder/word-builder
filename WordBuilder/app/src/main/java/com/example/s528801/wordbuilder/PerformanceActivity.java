package com.example.s528801.wordbuilder;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;

import java.util.ArrayList;


public class PerformanceActivity extends AppCompatActivity {

    private int[] yData ;//= {1,3};
    private String[] xData = {"Correct", "Wrong"};
    PieChart pieChart;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_performance);

        yData = new int[]{Score.wrong, Score.correct};

        pieChart = (PieChart) findViewById(R.id.pieChart);
        pieChart.setDescription(null);
        pieChart.setRotationEnabled(true);
        pieChart.setEnabled(true);
        pieChart.setHoleRadius(30f);
        pieChart.setTransparentCircleAlpha(0);
        pieChart.setCenterText("Performance chart");
        pieChart.setCenterTextSize(15);

        addDataSet();

        Button btn = (Button) findViewById(R.id.quizbutton);
      //  btn.setText("Back");
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent ini = new Intent(getApplicationContext(), QuizLevel.class);
                ini.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                ini.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(ini);
            }
        });

    }





    private void addDataSet() {
        //System.out.println("addDataSet started");
        ArrayList<PieEntry> yEntrys = new ArrayList<>();
        ArrayList<String> xEntrys = new ArrayList<>();

        for(int i = 0; i < yData.length; i++){
            yEntrys.add(new PieEntry(yData[i] , i));
        }

        for(int i = 1; i < xData.length; i++){
            xEntrys.add(xData[i]);
        }

        //create the data set
        PieDataSet pieDataSet = new PieDataSet(yEntrys, "User Performance");
        pieDataSet.setSliceSpace(3);
        pieDataSet.setValueTextSize(15);

        //add colors to dataset
        ArrayList<Integer> colors = new ArrayList<>();
        colors.add(Color.RED);
        colors.add(Color.GREEN);
        pieDataSet.setColors(colors);

        //add legend to chart
        Legend legend = pieChart.getLegend();
        legend.setForm(Legend.LegendForm.CIRCLE);

        //create pie data object
        PieData pieData = new PieData(pieDataSet);
        pieChart.setData(pieData);
        pieChart.invalidate();
    }
}
