package com.example.s528801.wordbuilder;

/**
 * Created by s528765 on 11/17/2017.
 */

public class WordsAndMeaning3 {
    public String word;
    public String meaning;

    @Override
    public String toString() {
        return "WordsAndMeanings{" +
                "word='" + word + '\'' +
                ", meaning='" + meaning + '\'' +
                '}';
    }
}
