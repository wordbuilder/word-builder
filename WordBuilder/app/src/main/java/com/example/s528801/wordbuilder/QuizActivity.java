package com.example.s528801.wordbuilder;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class QuizActivity extends AppCompatActivity {


    private Questions mQuestionLibrary = new Questions();

    private TextView mScoreView;
    private TextView mQuestionView;
    private Button mButtonChoice1;
    private Button mButtonChoice2;
    private Button mButtonChoice3;

    private String mAnswer;
    private int mScore = 0;
    private int mQuestionNumber = 0;
    private static int noOfQues = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);

        noOfQues = 0;
        int level = QuizLevel.level;
        switch (level) {
            case 1:
                mQuestionNumber = 0;
                break;
            case 2:
                mQuestionNumber = 3;
                break;
            case 3:
                mQuestionNumber = 6;
                break;
            case 4:
                mQuestionNumber = 9;
                break;
            case 5:
                mQuestionNumber = 12;
                break;
            case 6:
                mQuestionNumber = 15;
                break;
            case 7:
                mQuestionNumber = 18;
                break;
            case 8:
                mQuestionNumber = 21;
                break;
            case 9:
                mQuestionNumber = 24;
                break;
            case 10:
                mQuestionNumber = 27;
                break;
            case 11:
                mQuestionNumber = 30;
                break;
            case 12:
                mQuestionNumber = 33;
                break;
            case 13:
                mQuestionNumber = 36;
                break;
            case 14:
                mQuestionNumber = 39;
                break;
            case 15:
                mQuestionNumber = 42;
                break;
            case 16:
                mQuestionNumber = 45;
                break;
            case 17:
                mQuestionNumber = 48;
                break;
            case 18:
                mQuestionNumber = 51;
                break;
            case 19:
                mQuestionNumber = 54;
                break;
            case 20:
                mQuestionNumber = 57;
                break;
            case 21:
                mQuestionNumber = 60;
                break;
            case 22:
                mQuestionNumber = 63;
                break;
            case 23:
                mQuestionNumber = 66;
                break;
            case 24:
                mQuestionNumber = 69;
                break;
            case 25:
                mQuestionNumber = 72;
                break;
            case 26:
                mQuestionNumber = 75;
                break;
            case 27:
                mQuestionNumber = 78;
                break;
            case 28:
                mQuestionNumber = 81;
                break;
        }
        mScoreView = (TextView) findViewById(R.id.score);
        mQuestionView = (TextView) findViewById(R.id.question);
        mButtonChoice1 = (Button) findViewById(R.id.choice1);
        mButtonChoice2 = (Button) findViewById(R.id.choice2);
        mButtonChoice3 = (Button) findViewById(R.id.choice3);

        updateQuestion();

        final Score score = new Score(0, 0);


        //Start of Button Listener for Button1
        mButtonChoice1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mButtonChoice1.getText() == mAnswer) {
                    mScore = mScore + 1;
                    updateScore(mScore);
                    updateQuestion();
                    score.addCorrect();
                    Toast.makeText(QuizActivity.this, "correct", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(QuizActivity.this, "wrong", Toast.LENGTH_SHORT).show();
                    updateQuestion();
                    score.addWrong();
                }
            }
        });

        //End of Button Listener for Button1

        //Start of Button Listener for Button2
        mButtonChoice2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //My logic for Button goes in here

                if (mButtonChoice2.getText() == mAnswer) {
                    mScore = mScore + 1;
                    score.addCorrect();
                    updateScore(mScore);
                    updateQuestion();
                    //This line of code is optiona
                    Toast.makeText(QuizActivity.this, "correct", Toast.LENGTH_SHORT).show();

                } else {
                    Toast.makeText(QuizActivity.this, "wrong", Toast.LENGTH_SHORT).show();
                    score.addWrong();
                    updateQuestion();
                }
            }
        });

        //End of Button Listener for Button2


        //Start of Button Listener for Button3
        mButtonChoice3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //My logic for Button goes in here

                if (mButtonChoice3.getText() == mAnswer) {
                    mScore = mScore + 1;
                    updateScore(mScore);
                    updateQuestion();
                    score.addCorrect();
                    //This line of code is optiona
                    Toast.makeText(QuizActivity.this, "correct", Toast.LENGTH_SHORT).show();

                } else {
                    Toast.makeText(QuizActivity.this, "wrong", Toast.LENGTH_SHORT).show();
                    updateQuestion();
                    score.addWrong();
                }
            }
        });

    }

    private void updateQuestion() {
        mQuestionView.setText(mQuestionLibrary.getQuestion(mQuestionNumber));
        mButtonChoice1.setText(mQuestionLibrary.getChoice1(mQuestionNumber));
        mButtonChoice2.setText(mQuestionLibrary.getChoice2(mQuestionNumber));
        mButtonChoice3.setText(mQuestionLibrary.getChoice3(mQuestionNumber));

        mAnswer = mQuestionLibrary.getCorrectAnswer(mQuestionNumber);
        if (noOfQues < 3) {
            mQuestionNumber++;
            noOfQues++;
        } else {
            mQuestionView.setText("Test Completed. Please click on the score to view your percentage.\nPlease click quit to go back.");
            mButtonChoice1.setText("Performance");
            mButtonChoice1.setOnClickListener(new View.OnClickListener() {
                                                  @Override
                                                  public void onClick(View view) {
                                                      progress();
                                                  }
                                              }


            );
            mButtonChoice2.setVisibility(View.GONE);
            mButtonChoice3.setVisibility(View.GONE);


        }
    }

    private void progress() {
        Intent startprogress = new Intent(this, PerformanceActivity.class);
        startActivity(startprogress);
    }


    private void updateScore(int point) {
        mScoreView.setText("" + mScore);

    }

    public void quitTest(View v) {
        finish();
    }



}
