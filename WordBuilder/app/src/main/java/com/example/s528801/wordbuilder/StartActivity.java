package com.example.s528801.wordbuilder;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class StartActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
    }

    public void startLearning(View v)
    {
        Intent startlearning = new Intent(this,WelcomeActivity.class);
        startActivity(startlearning);
    }
    public void takeQuiz(View v)
    {
        Intent takeQuiz = new Intent(this,QuizSelection.class);
        startActivity(takeQuiz);
    }
    public void needHelp(View v)
    {
        Intent needHelp = new Intent(this,InstructionsActivity.class);
        startActivity(needHelp);
    }

}