package com.example.s528801.wordbuilder;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;

import com.backendless.Backendless;
import com.backendless.IDataStore;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;
import com.backendless.persistence.DataQueryBuilder;

import java.util.ArrayList;
import java.util.List;

public class LevelLearning extends AppCompatActivity {
    boolean onBeginner = false;
    boolean onIntermediate = false;
    boolean onAdvanced = false;
    int clickCount = 0;
    int clickCountI = 0;
    int clickCountA = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_level_learning);
        int Lcourse = WelcomeActivity.Lcourse;
        Backendless.setUrl(Defaults.SERVER_URL);
        Backendless.initApp(getApplicationContext(), Defaults.APPLICATION_ID, Defaults.API_KEY);


        switch (Lcourse) {
            case 1:
                final String[] words = new String[200];
                final ArrayList<String> wordsList = new ArrayList<>();
                final ArrayList<WordsAndMeanings> arrayListData1 = new ArrayList<>();
                final String[] items = {
                        "Word      : Avide\nMeaning: Greedy",
                        "Word      : Abject\nMeaning: Lacking pride",
                        "Word      : Cerebral\nMeaning: Intellectual",
                        "Word      : Douse\nMeaning: Plunge into water",
                        "Word      : Chalice\nMeaning: Consecrated cup",
                        "Word      : Morose\nMeaning: Gloomy",
                        "Word      : Vacillate\nMeaning: To oscillate",
                        "Word      : Impromptu\nMeaning: With no preparation",
                        "Word      : Attenuate\nMeaning: Toreduce",
                        "Word      : Halting\nMeaning: Pause",
                };
                Log.d("TAG", "hello");
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_expandable_list_item_1,items);

                ListView lv = (ListView) findViewById(R.id.listView);
                lv.setAdapter(adapter);
                for (int i = 0; i < items.length; i++) {
                    WordsAndMeanings wm = new WordsAndMeanings();
                    String[] array = items[i].split("\n");
                    wm.word = array[0].substring(6);
                    wm.meaning = array[1].substring(9);
                    // wm.rowNumber = i + 1;
                    arrayListData1.add(wm);
                    Backendless.Data.of(WordsAndMeanings.class).save(wm, new AsyncCallback<WordsAndMeanings>() {

                        @Override
                        public void handleResponse(WordsAndMeanings response) {
                            Log.d("DB", "saved" + response);
                        }

                        @Override
                        public void handleFault(BackendlessFault fault) {
                            Log.e("Error", "Occured" + fault.getMessage());
                        }
                    });

                }

                IDataStore<WordsAndMeanings> orderStorage = Backendless.Data.of(WordsAndMeanings.class);
                DataQueryBuilder query=DataQueryBuilder.create();
                orderStorage.find(query, new AsyncCallback<List<WordsAndMeanings>>() {

                    @Override
                    public void handleResponse(List<WordsAndMeanings> response) {

                        Log.d("Printing : ","response Details: "+response);
                        Object[] reponsehere = response.toArray();
                        ArrayList<DataRetreived> adapaterdata = new ArrayList<>();

                        for (int i = 0; i<reponsehere.length;i++)
                        {
                           // Log.d("here","here"+reponsehere[i]);
                            String str = String.valueOf(reponsehere[i]);
                            String[] strArray = str.split(",");
                            Log.d("str","array "+strArray[0].substring(22));
                            Log.d("str","mean"+strArray[1].substring(strArray[1].indexOf("=")+1,(strArray[1].indexOf("}"))));
                            DataRetreived dr = new DataRetreived();
                            String prtWord = strArray[0].substring(22);
                            String prtMean = strArray[1].substring(strArray[1].indexOf("=")+1,(strArray[1].indexOf("}")));
                            dr.word = prtWord.substring(prtWord.indexOf("'")+1,prtWord.lastIndexOf("'"));
                            dr.meaning = prtMean;
                            words[i] = dr.word+"\n"+dr.meaning;

                            Log.d("dr","word "+dr.word);
                            Log.d("dr","mean"+dr.meaning);
                            adapaterdata.add(dr);


                        }

                        //Log.d("array","here"+words.toString());



                    }

                    @Override
                    public void handleFault(BackendlessFault fault) {
                        Log.e( "Error", "Occured" + fault.getMessage() );
                    }
                });

           onBeginner = true;
           onIntermediate = false;
           onAdvanced = false;


            break;
            case 2:
                final String[] words2 = new String[200];
                final ArrayList<WordsAndMeaning2> arrayListData2 = new ArrayList<>();
                String[] items2 = {
                        "Word      : Convoke\nMeaning: To call together",
                        "Word      : Nonchalant\nMeaning: Unconcerned",
                        "Word      : Reprove\nMeaning: Scold",
                        "Word      : Repartee\nMeaning: A witty reply",
                        "Word      : Deride\nMeaning: Redicule",
                        "Word      : Shun\nMeaning: First of its kind in a region",
                        "Word      : Jovial\nMeaning: Cheerful",
                        "Word      : Limber\nMeaning: Flexible",
                        "Word      : Chic\nMeaning: Stylish",
                };
                ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(this, android.R.layout.simple_expandable_list_item_1, items2);
                ListView lv2 = (ListView) findViewById(R.id.listView);
                lv2.setAdapter(adapter2);
                Log.d("TAG", "hello");
                for (int i = 0; i < items2.length; i++) {
                    WordsAndMeaning2 wm2 = new WordsAndMeaning2();
                    String[] array = items2[i].split("\n");
                    wm2.word = array[0].substring(6);
                    wm2.meaning = array[1].substring(9);
                    arrayListData2.add(wm2);
                    Backendless.Data.of(WordsAndMeaning2.class).save(wm2, new AsyncCallback<WordsAndMeaning2>() {


                        @Override
                        public void handleResponse(WordsAndMeaning2 response) {
                            Log.d("DataBase is being", "saved" + response);
                        }

                        @Override
                        public void handleFault(BackendlessFault fault) {

                            Log.d("Error has","occoured");
                        }
                    });

                }
                IDataStore<WordsAndMeanings> orderStorage1 = Backendless.Data.of(WordsAndMeanings.class);
                DataQueryBuilder query1=DataQueryBuilder.create();
                orderStorage1.find(query1, new AsyncCallback<List<WordsAndMeanings>>() {

                    @Override
                    public void handleResponse(List<WordsAndMeanings> response) {

                        Log.d("Printing : ","response Details: "+response);
                        Object[] reponsehere = response.toArray();
                        ArrayList<DataRetreived> adapaterdata = new ArrayList<>();

                        for (int i = 0; i<reponsehere.length;i++)
                        {
                            Log.d("here","here"+reponsehere[i]);
                            String str = String.valueOf(reponsehere[i]);
                            String[] strArray = str.split(",");
                            Log.d("str","array "+strArray[0].substring(22));
                            Log.d("str","mean"+strArray[1].substring(strArray[1].indexOf("=")+1,(strArray[1].indexOf("}"))));
                            DataRetreived dr = new DataRetreived();
                            String prtWord = strArray[0].substring(22);
                            String prtMean = strArray[1].substring(strArray[1].indexOf("=")+1,(strArray[1].indexOf("}")));
                            dr.word = prtWord.substring(prtWord.indexOf("'")+1,prtWord.lastIndexOf("'"));
                            dr.meaning = prtMean;

                            words2[i] = dr.word+"\n"+dr.meaning;

                            Log.d("dr","word "+dr.word);
                            Log.d("dr","mean"+dr.meaning);
                            adapaterdata.add(dr);


                        }

                        Log.d("array","here"+words2.toString());



                    }

                    @Override
                    public void handleFault(BackendlessFault fault) {
                        Log.e( "MYAPP", "Server reported an error " + fault.getMessage() );
                    }
                });

//
                onIntermediate = true;
                onBeginner = false;
                onAdvanced = false;
                break;



            case 3:
                final String[] words3 = new String[200];
                final ArrayList<WordsAndMeaning3> arrayListData3 = new ArrayList<>();
                String[] items3 = {
                        "Word      : Throng\nMeaning: Crowd",
                        "Word      : Sartorial\nMeaning: Tailoring",
                        "Word      : outstrip\nMeaning: Outrun",
                        "Word      : Engross\nMeaning: Absorb",
                        "Word      : outwit\nMeaning: Outsmart",
                        "Word      : Hasten\nMeaning: Hurry",
                        "Word      : Paragon\nMeaning: Model of excellence",
                        "Word      : Stodgy\nMeaning: Stuffy",
                        "Word      : Guffaw\nMeaning: Laughter",
                        "Word      : Babble\nMeaning: Meaningless talk",
                };
//                ArrayAdapter<String> adapter3 = new ArrayAdapter<String>(this, android.R.layout.simple_expandable_list_item_1, items3);
//                ListView lv3 = (ListView) findViewById(R.id.listView);
//                lv3.setAdapter(adapter3);
                Log.d("TAG", "hello");
                ArrayAdapter<String> adapter3 = new ArrayAdapter<String>(this, android.R.layout.simple_expandable_list_item_1, items3);
                ListView lv3 = (ListView) findViewById(R.id.listView);
                lv3.setAdapter(adapter3);
                Log.d("TAG", "hello");
                for (int i = 0; i < items3.length; i++) {
                    WordsAndMeaning3 wm3 = new WordsAndMeaning3();
                    String[] array = items3[i].split("\n");
                    wm3.word = array[0].substring(6);
                    wm3.meaning = array[1].substring(9);
                    // wm.rowNumber = i + 1;
                    arrayListData3.add(wm3);
                    Backendless.Data.of(WordsAndMeaning3.class).save(wm3, new AsyncCallback<WordsAndMeaning3>() {


                        @Override
                        public void handleResponse(WordsAndMeaning3 response) {
                            Log.d("DB", "saved" + response);
                        }

                        @Override
                        public void handleFault(BackendlessFault fault) {

                        }
                    });

                }
                IDataStore<WordsAndMeaning3> orderStorage3 = Backendless.Data.of(WordsAndMeaning3.class);
                DataQueryBuilder query3=DataQueryBuilder.create();
                orderStorage3.find(query3, new AsyncCallback<List<WordsAndMeaning3>>() {

                    @Override
                    public void handleResponse(List<WordsAndMeaning3> response) {

                        Log.d("Printing : ","response Details: "+response);
                        Object[] reponsehere = response.toArray();
                        ArrayList<DataRetreived> adapaterdata = new ArrayList<>();

                        for (int i = 0; i<reponsehere.length;i++)
                        {
                            Log.d("here","here"+reponsehere[i]);
                            String str = String.valueOf(reponsehere[i]);
                            String[] strArray = str.split(",");
                            Log.d("str","array "+strArray[0].substring(22));
                            Log.d("str","mean"+strArray[1].substring(strArray[1].indexOf("=")+1,(strArray[1].indexOf("}"))));
                            DataRetreived dr = new DataRetreived();
                            String prtWord = strArray[0].substring(22);
                            String prtMean = strArray[1].substring(strArray[1].indexOf("=")+1,(strArray[1].indexOf("}")));
                            dr.word = prtWord.substring(prtWord.indexOf("'")+1,prtWord.lastIndexOf("'"));
                            dr.meaning = prtMean;
                            words3[i] = dr.word+"\n"+dr.meaning;

                            Log.d("dr","word "+dr.word);
                            Log.d("dr","mean"+dr.meaning);
                            adapaterdata.add(dr);


                        }

                        Log.d("array","here"+words3.toString());



                    }

                    @Override
                    public void handleFault(BackendlessFault fault) {
                        Log.e( "MYAPP", "Server reported an error " + fault.getMessage() );
                    }
                });
                onAdvanced  = true;
                onBeginner = false;
                onIntermediate = false;
                break;

        }
        Button btn = (Button) findViewById(R.id.skipbutton);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent ini = new Intent(getApplicationContext(), WelcomeActivity.class);
                ini.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                ini.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(ini);
            }
        });

        ImageView imgview = (ImageView)findViewById(R.id.imageView);
        imgview.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view)
            {
                Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.google.com"));
                startActivity(i);

            }
        });




    }
    public void retrieval(View v)
    {
        IDataStore<WordsAndMeanings> orderStorage = Backendless.Data.of(WordsAndMeanings.class);
        DataQueryBuilder query=DataQueryBuilder.create();
        orderStorage.find(query, new AsyncCallback<List<WordsAndMeanings>>() {

            @Override
            public void handleResponse(List<WordsAndMeanings> response) {
                Log.d("Printing : ","Order Details: "+response);
            }

            @Override
            public void handleFault(BackendlessFault fault) {
                Log.e( "MYAPP", "Server reported an error " + fault.getMessage() );
            }
        });
    }
    public void onclickB(View v) {
        if(onBeginner) {
            if (clickCount == 0) {
                final String[] wordsB = new String[200];
                final ArrayList<String> wordsListB = new ArrayList<>();
                final ArrayList<WordsAndMeanings> arrayListDataB = new ArrayList<>();
                final String[] itemsB = {
                        "Word      : Gainsay\nMeaning: Take exception to",
                        "Word      : Aberrant\nMeaning:  Markedly different",
                        "Word      : Magnanimity\nMeaning: High-minded",
                        "Word      : Contention\nMeaning: Disagreement",
                        "Word      : Malingerer\nMeaning: Illness",
                        "Word      : Certitude\nMeaning: Confidence",
                        "Word      : Jaded\nMeaning: Worn out",
                        "Word      : Raspy\nMeaning: Rough; grating",
                        "Word      : Feral\nMeaning: Wild",
                        "Word      : Hubris\nMeaning: Arrogance",

                };
                Log.d("TAG", "helloooooooooo");
                ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(this, android.R.layout.simple_expandable_list_item_1, itemsB);
                ListView lv = (ListView) findViewById(R.id.listView);
                lv.setAdapter(adapter2);
                clickCount++;
            } else {
                final String[] wordsB1 = new String[200];
                final ArrayList<String> wordsListB1 = new ArrayList<>();
                final ArrayList<WordsAndMeanings> arrayListDataB1 = new ArrayList<>();
                final String[] itemsB1 = {
                        "Word      : Diatribe\nMeaning: Verbal attack",
                        "Word      : Diffuse\nMeaning:  Spread out",
                        "Word      : Brassy\nMeaning: Showy",
                        "Word      : Truss\nMeaning: Framework",
                        "Word      : Subside\nMeaning: Wear off",
                        "Word      : Askew\nMeaning: Turned ",
                        "Word      : Bombast \nMeaning: Pretentious",
                        "Word      : Distraught\nMeaning: Deeply agitated",
                        "Word      : Inebriate\nMeaning: Intoxicate",
                        "Word      : Infantile\nMeaning: Infantlike",

                };
                Log.d("TAG", "helloooooooooo");
                ArrayAdapter<String> adapter3 = new ArrayAdapter<String>(this, android.R.layout.simple_expandable_list_item_1, itemsB1);
                ListView lv = (ListView) findViewById(R.id.listView);
                lv.setAdapter(adapter3);
                Button btn = (Button) findViewById(R.id.buttonb);
                btn.setText("Back");
                btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent ini = new Intent(getApplicationContext(), WelcomeActivity.class);
                        ini.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        ini.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(ini);
                    }
                });

            }
        }
        else if(onIntermediate)
        {
            if (clickCountI == 0) {
                final String[] wordsI = new String[200];
                final ArrayList<String> wordsListI = new ArrayList<>();
                final ArrayList<WordsAndMeanings> arrayListDataI = new ArrayList<>();
                final String[] itemsI = {
                        "Word      : Inept\nMeaning: Unsuited",
                        "Word      : Influx\nMeaning: Flowing into",
                        "Word      : Insatiable\nMeaning: Unquenchable",
                        "Word      : Subterfuge\nMeaning: A deceptive device",
                        "Word      : Winsome\nMeaning: Charming",
                        "Word      : Falter\nMeaning: Hesitate ",
                        "Word      : Rabble \nMeaning: An unruly crowd",
                        "Word      : Smirk\nMeaning: Self-satisfied smile",
                        "Word      : Earshot\nMeaning:  Hearing distance",
                        "Word      : Acquiesce\nMeaning: To accept",

                };
                Log.d("TAG", "helloooooooooo");
                ArrayAdapter<String> adapter3 = new ArrayAdapter<String>(this, android.R.layout.simple_expandable_list_item_1, itemsI);
                ListView lv = (ListView) findViewById(R.id.listView);
                lv.setAdapter(adapter3);
                clickCountI++;
            }
            else {
                final String[] wordsI1 = new String[200];
                final ArrayList<String> wordsListI1 = new ArrayList<>();
                final ArrayList<WordsAndMeanings> arrayListDataI1 = new ArrayList<>();
                final String[] itemsI1 = {
                        "Word      : Extrinsic\nMeaning: External",
                        "Word      : Supercilious\nMeaning:  Disdainful",
                        "Word      : Stultify\nMeaning: Render useless",
                        "Word      : Vaunt\nMeaning: To brag",
                        "Word      : Desultory\nMeaning: Irregular",
                        "Word      : Tension\nMeaning: Stretching ",
                        "Word      : Testator \nMeaning: Maker of a will",
                        "Word      : Insinuate\nMeaning: Give to understand",
                        "Word      : Penchant\nMeaning: A strong liking",
                        "Word      : Perfidious\nMeaning: Tending to betray",

                };
                Log.d("TAG", "helloooooooooo");
                ArrayAdapter<String> adapter3 = new ArrayAdapter<String>(this, android.R.layout.simple_expandable_list_item_1, itemsI1);
                ListView lv = (ListView) findViewById(R.id.listView);
                lv.setAdapter(adapter3);

                Button btn = (Button) findViewById(R.id.buttonb);
                btn.setText("Back");
                btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent ini = new Intent(getApplicationContext(), WelcomeActivity.class);
                        ini.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        ini.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(ini);
                    }
                });
            }
        }
        else if (onAdvanced)
        {

            if (clickCountA == 0) {
                final String[] wordsIA = new String[200];
                final ArrayList<String> wordsListA = new ArrayList<>();
                final ArrayList<WordsAndMeanings> arrayListDataA = new ArrayList<>();
                final String[] itemsI = {
                        "Word      : Burgeon\nMeaning: Flourish",
                        "Word      : Intractable\nMeaning: Difficult to mold",
                        "Word      : Intransigence\nMeaning: Refusal to change",
                        "Word      : Cacophonous\nMeaning: Unpleasant sound",
                        "Word      : Irascible \nMeaning: Aroused to anger",
                        "Word      : Perfidious\nMeaning: Tending to betray ",
                        "Word      : Plummet \nMeaning: Drop sharply",
                        "Word      : Stigma\nMeaning: Symbol of disgrace",
                        "Word      : Discommode\nMeaning: Strong liking-",
                        "Word      : Mollify\nMeaning: Soothe",

                };
                Log.d("TAG", "helloooooooooo");
                ArrayAdapter<String> adapter3 = new ArrayAdapter<String>(this, android.R.layout.simple_expandable_list_item_1, itemsI);
                ListView lv = (ListView) findViewById(R.id.listView);
                lv.setAdapter(adapter3);
                clickCountA++;
            }
            else {
                final String[] wordsA1 = new String[200];
                final ArrayList<String> wordsListA1 = new ArrayList<>();
                final ArrayList<WordsAndMeanings> arrayListDataA1 = new ArrayList<>();
                final String[] itemsI1 = {
                        "Word      : Protract\nMeaning: Irregular",
                        "Word      : Banal\nMeaning:  Commonplace",
                        "Word      : Preen \nMeaning: Passion",
                        "Word      : Porous\nMeaning: Full of holes",
                        "Word      : Palmy\nMeaning: Prosperous",
                        "Word      : Proscribe\nMeaning: Command against ",
                        "Word      : Dissension \nMeaning: Disagreement",
                        "Word      : Glut \nMeaning: Oversupply",
                        "Word      : Surreptitious\nMeaning: Secret",
                        "Word      : Ardor\nMeaning: Passion",

                };
                Log.d("TAG", "helloooooooooo");
                ArrayAdapter<String> adapter3 = new ArrayAdapter<String>(this, android.R.layout.simple_expandable_list_item_1, itemsI1);
                ListView lv = (ListView) findViewById(R.id.listView);
                lv.setAdapter(adapter3);

                Button btn = (Button) findViewById(R.id.buttonb);
                btn.setText("Back");
                btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent ini = new Intent(getApplicationContext(), WelcomeActivity.class);
                        ini.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        ini.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(ini);
                    }
                });
            }
        }
    }

//    public void onClickI(View v) {
//        if (clickCountI == 0) {
//            final String[] wordsI = new String[200];
//            final ArrayList<String> wordsListI = new ArrayList<>();
//            final ArrayList<WordsAndMeanings> arrayListDataI = new ArrayList<>();
//            final String[] itemsI = {
//                    "Word      : Inept\nMeaning: Unsuited",
//                    "Word      : Influx\nMeaning: Flowing into",
//                    "Word      : Insatiable\nMeaning: Unquenchable",
//                    "Word      : Subterfuge\nMeaning: A deceptive device",
//                    "Word      : Winsome\nMeaning: Charming",
//                    "Word      : Falter\nMeaning: Hesitate ",
//                    "Word      : Rabble \nMeaning: An unruly crowd",
//                    "Word      : Smirk\nMeaning: Self-satisfied smile",
//                    "Word      : Earshot\nMeaning:  Hearing distance",
//                    "Word      : Acquiesce\nMeaning: To accept",
//
//            };
//            Log.d("TAG", "helloooooooooo");
//            ArrayAdapter<String> adapter3 = new ArrayAdapter<String>(this, android.R.layout.simple_expandable_list_item_1, itemsI);
//            ListView lv = (ListView) findViewById(R.id.listView);
//            lv.setAdapter(adapter3);
//            clickCountI++;
//        }
//        else{
//            final String[] wordsI1 = new String[200];
//            final ArrayList<String> wordsListI1 = new ArrayList<>();
//            final ArrayList<WordsAndMeanings> arrayListDataI1 = new ArrayList<>();
//            final String[] itemsI1 = {
//                    "Word      : Extrinsic\nMeaning: External",
//                    "Word      : Supercilious\nMeaning:  Disdainful",
//                    "Word      : Stultify\nMeaning: Render useless",
//                    "Word      : Vaunt\nMeaning: To brag",
//                    "Word      : Desultory\nMeaning: Irregular",
//                    "Word      : Tension\nMeaning: Stretching ",
//                    "Word      : Testator \nMeaning: Maker of a will",
//                    "Word      : Insinuate\nMeaning: Give to understand",
//                    "Word      : Penchant\nMeaning: A strong liking",
//                    "Word      : Perfidious\nMeaning: Tending to betray",
//
//            };
//            Log.d("TAG", "helloooooooooo");
//            ArrayAdapter<String> adapter3 = new ArrayAdapter<String>(this, android.R.layout.simple_expandable_list_item_1, itemsI1);
//            ListView lv = (ListView) findViewById(R.id.listView);
//            lv.setAdapter(adapter3);
//        }
//        Button btn = (Button) findViewById(R.id.buttonb);
//        btn.setText("Back");
//        btn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent ini = new Intent(getApplicationContext(), WelcomeActivity.class);
//                ini.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
//                ini.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                startActivity(ini);
//            }
//        });
//    }
    public void onClickA(View v) {
        if (clickCountA == 0) {
            final String[] wordsIA = new String[200];
            final ArrayList<String> wordsListA = new ArrayList<>();
            final ArrayList<WordsAndMeanings> arrayListDataA = new ArrayList<>();
            final String[] itemsI = {
                    "Word      : Burgeon\nMeaning: Flourish",
                    "Word      : Intractable\nMeaning: Difficult to mold",
                    "Word      : Intransigence\nMeaning: Refusal to change",
                    "Word      : Cacophonous\nMeaning: Unpleasant sound",
                    "Word      : Irascible \nMeaning: Quickly aroused to anger",
                    "Word      : Perfidious\nMeaning: Tending to betray ",
                    "Word      : Plummet \nMeaning: Drop sharply",
                    "Word      : Stigma\nMeaning: Symbol of disgrace",
                    "Word      : Discommode\nMeaning: Strong liking-",
                    "Word      : Mollify\nMeaning: Soothe",

            };
            Log.d("TAG", "helloooooooooo");
            ArrayAdapter<String> adapter3 = new ArrayAdapter<String>(this, android.R.layout.simple_expandable_list_item_1, itemsI);
            ListView lv = (ListView) findViewById(R.id.listView);
            lv.setAdapter(adapter3);
            clickCountA++;
        }
        else{
            final String[] wordsA1 = new String[200];
            final ArrayList<String> wordsListA1 = new ArrayList<>();
            final ArrayList<WordsAndMeanings> arrayListDataA1 = new ArrayList<>();
            final String[] itemsI1 = {
                    "Word      : Protract\nMeaning: Irregular",
                    "Word      : Banal\nMeaning:  Commonplace",
                    "Word      : Preen \nMeaning: Passion",
                    "Word      : Porous\nMeaning: Full of holes",
                    "Word      : Palmy\nMeaning: Prosperous",
                    "Word      : Proscribe\nMeaning: Command against ",
                    "Word      : Dissension \nMeaning: Disagreement",
                    "Word      : Glut \nMeaning: Oversupply",
                    "Word      : Surreptitious\nMeaning: Secret",
                    "Word      : Ardor\nMeaning: Passion",

            };
            Log.d("TAG", "helloooooooooo");
            ArrayAdapter<String> adapter3 = new ArrayAdapter<String>(this, android.R.layout.simple_expandable_list_item_1, itemsI1);
            ListView lv = (ListView) findViewById(R.id.listView);
            lv.setAdapter(adapter3);
        }
        Button btn = (Button) findViewById(R.id.buttonb);
        btn.setText("Back");
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent ini = new Intent(getApplicationContext(), WelcomeActivity.class);
                ini.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                ini.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(ini);
            }
        });
    }



}





